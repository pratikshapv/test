<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DB0201EN-SkillsNetwork/labs/Labs_Coursera_V5/labs/Lab%20-%20INSERT%20-%20UPDATE%20-%20DELETE/images/IDSNlogo.png" width="200" height="200"> 

# Hands-on Lab : INSERT, UPDATE, DELETE

**Estimated time needed:** 20 minutes

In this lab, you will learn some commonly used DML (Data Manipulation Language) statements of SQL other than SELECT. First, you will learn the INSERT statement, which is used to insert new rows into a table. Next, you will learn the UPDATE statement which is used to update the data in existing rows in the table. Lastly, you will learn the DELETE statement which is used to remove rows from a table.


**How does the syntax of an INSERT statement look?**

 ```
 INSERT INTO table_name (column1, column2, ... )
 VALUES (value1, value2, ... )
;
 ```


**How does the syntax of an UPDATE statement look?**

```
UPDATE table_name
SET column1 = value1, column2 = value2, ...
WHERE condition
;
```


**How does the syntax of a DELETE statement look?**

```
DELETE FROM table_name
WHERE condition
;
```


# 

## Software Used in this Lab

In this lab, you will use <a href="https://www.mysql.com/?utm_medium=Exinfluencer&utm_source=Exinfluencer&utm_content=000026UJ&utm_term=10006555&utm_id=NA-SkillsNetwork-Channel-SkillsNetworkCoursesIBMDB0110ENSkillsNetwork24601058-2021-01-01">MySQL</a>. MySQL is a Relational Database Management System (RDBMS) designed to efficiently store, manipulate, and retrieve data.

<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/mysql.png" width="100" height="100">
<p></p>

To complete this lab you will utilize MySQL relational database service available as part of IBM Skills Network Labs (SN Labs) Cloud IDE. SN Labs is a virtual lab environment used in this course.

# 

## Database Used in this Lab

**Instructor** database has been used in this lab.

Here you will be using 1 dump file for this purpose.

 * <a href="https://gitlab.com/pratikshapv/test/-/blob/main/data/Instructor.sql">Instructor</a>

## Objectives

After completing this lab, you will be able to:

*   Insert new rows into a table
*   Update data in existing rows of the table
*   Remove rows from a table

# 

## Exercise

In this exercise through different tasks, you will learn how to create tables and load data in the MySQL database service using the phpMyAdmin graphical user interface (GUI) tool.

# 

### Task A: Create a database

1.  Go to **Terminal > New Terminal** to open a terminal from the side by side launched Cloud IDE.

    ![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/2.1.png)


2.  Start MySQL service session in the Cloud IDE using the command below in the terminal. Find your MySQL service session password from the highlighted location of the terminal shown in the image below. Note down your MySQL service session password because you may need to use it later in the lab.

    ```
    start_mysql
    ```


    ![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/2.2.png)


3.  Copy your phpMyAdmin weblink from the highlighted location of the terminal shown in the image below. Past it into the address bar in a new tab of your web browser. This will open the phpMyAdmin tool.

    ![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/2.3.png)


4.  You will see the phpMyAdmin GUI tool.

    ![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/2.4.png)


5.  In the tree-view, click **New** to create a new empty database. Then enter **Instructor** as the name of the database and click **Create**.

    The encoding will be left as **utf8mb4\_0900\_ai_ci**. UTF-8 is the most commonly used character encoding for content or data.

    Proceed to Task B.

    ![image](./images/db1.png)

6.  Load the dump files one by one into the database **Instructor**  by clicking the **Import** tab and choose the file.
Click on **Go** button.

    ![image](./images/dump1.png)

    ![image](./images/dump2.png)

# Exploring the Database

Let us first explore the **Instructors** database using the **Datasette** tool:

1.  Copy the code below by clicking on the little copy button on the bottom right of the codeblock below and then paste it into the SQL section of the phpMyAdmin tool using either **Ctrl+V** or right-click in the text box and choose **Paste**. Click on **Go** to view the result.
    ```
    SELECT * FROM Instructor;
    ```


     ![image](./images/exploring_db1.png)

2.  Click **Go**.

3.  Now you can scroll down the table and explore all the columns and rows of the **Instructor** table to get an overall idea of the table contents.

     ![image](./images/exploring_db2.png)

4.  These are the column attribute descriptions from the **Instructor** table:

    ```
    Instructor (
        ins_id:     unique identification number of the instructors,    
        lastname:   last name of the instructors,
        firstname:  first name of the instructors,
        city:       name of the cities where instructors are located,
        country:    two-letter country code of the countries where instructors are located
    )
    ```


# Exercise 1: INSERT

In this exercise, you will first go through some examples of using INSERT in queries and then solve some exercise problems by using it.

# 

## Task A: Example exercises on INSERT

Let us go through some examples of INSERT related queries:


1.  In this example, suppose we want to insert a new single row into the **Instructor** table.

    1.  Problem:

        > *Insert a new instructor record with id 4 for Sandip Saha who lives in Edmonton, CA into the "Instructor" table.*

    2.  Solution:

        ```
        INSERT INTO Instructor(ins_id, lastname, firstname, city, country)
        VALUES(4, 'Saha', 'Sandip', 'Edmonton', 'CA');
        ```



    3.  Copy the solution code above by clicking on the little copy button on the bottom right of the codeblock below and paste it to tthe sql section of phpMyAdmin tool. Then click **Go**.

        ![image](./images/insert_a1.1.png)


    4.  Copy the code below by clicking on the little copy button on the bottom right of the codeblock below and paste it to the sql section of phpMyAdmin tool. Then click **Go**.


        ```
        SELECT * FROM Instructor;
        ```


    5.  Your output resultset should look like the image below:

         ![image](./images/insert_a1.2.png)



2.  In this example, suppose we want to insert some new multiple rows into the **Instructor** table.

    1.  Problem:

        > *Insert two new instructor records into the "Instructor" table. First record with id 5 for John Doe who lives in Sydney, AU. Second record with id 6 for Jane Doe who lives in Dhaka, BD.*

    2.  Solution:

        ```
        INSERT INTO Instructor(ins_id, lastname, firstname, city, country)
        VALUES(5, 'Doe', 'John', 'Sydney', 'AU'), (6, 'Doe', 'Jane', 'Dhaka', 'BD');
        ```



    3.  Copy the solution code above by clicking on the little copy button on the bottom right of the codeblock below and paste it to the sql section of phpMyAdmin tool. Then click **Go**.

        ![image](./images/insert_a2.1.png)




    4.  Copy the code below by clicking on the little copy button on the bottom right of the codeblock below and paste it to the sql section of phpMyAdmin tool. Then click **Go**.


        ```
        SELECT * FROM Instructor;
        ```


    5.  Your output resultset should look like the image below:

         ![image](./images/insert_a2.2.png)



# 

## Task B: Practice exercises on INSERT

Now, let us practice creating and running some INSERT related queries.

1.  Problem:

    > *Insert a new instructor record with id 7 for Antonio Cangiano who lives in Vancouver, CA into the "Instructor" table.*

     <details>
     <summary>Hint</summary>

    > Follow example 1 of the INSERT exercise.

     </details>

     <details>
     <summary>Solution</summary>

    ```
    INSERT INTO Instructor(ins_id, lastname, firstname, city, country)
    VALUES(7, 'Cangiano', 'Antonio', 'Vancouver', 'CA');

    SELECT * FROM Instructor;
    ```


     </details>

     <details>
     <summary>Output</summary>

     ![image](./images/insert_b1.png)
     </details>        


2.  Problem:

    > *Insert two new instructor records into the "Instructor" table. First record with id 8 for Steve Ryan who lives in Barlby, GB. Second record with id 9 for Ramesh Sannareddy who lives in Hyderabad, IN.*

     <details>
     <summary>Hint</summary>

    > Follow example 2 of the INSERT exercise.

     </details>

     <details>
     <summary>Solution</summary>

    ```
    INSERT INTO Instructor(ins_id, lastname, firstname, city, country)
    VALUES(8, 'Ryan', 'Steve', 'Barlby', 'GB'), (9, 'Sannareddy', 'Ramesh', 'Hyderabad', 'IN');

    SELECT * FROM Instructor;
    ```

     </details>

     <details>
     <summary>Output</summary>

     ![image](./images/insert_b2.png)

     </details>

# Exercise 2: UPDATE

In this exercise, you will first go through some examples of using UPDATE in queries and then solve some exercise problems by using it.

# 

## Task A: Example exercises on UPDATE

Let us go through some examples of UPDATE related queries:

1.  In this example, we want to update one column of an existing row of the table.

    1.  Problem:

        > *Update the city for Sandip to Toronto.*

    2.  Solution:

        ```
        UPDATE Instructor 
        SET city='Toronto' 
        WHERE firstname="Sandip";
        ```


    3.  Copy the solution code above by clicking on the little copy button on the bottom right of the codeblock below and paste it to the sql section of phpMyAdmin tool. Then click **Go**.

        ![image](./images/update_a1.1.png)

     <br>

    4.  Copy the code below by clicking on the little copy button on the bottom right of the codeblock below and paste it to the the sql section of phpMyAdmin tool. Then click **Go**.

        ```
        SELECT * FROM Instructor;
        ```


    5.  Your output resultset should look like the image below:

         ![image](./images/update_a1.2.png)


2.  In this example, we want to update multiple columns of an existing row of the table.

    1.  Problem:

        > *Update the city and country for Doe with id 5 to Dubai and AE respectively.*

    2.  Solution:

        ```
        UPDATE Instructor 
        SET city='Dubai', country='AE' 
        WHERE ins_id=5;
        ```



    3.  Copy the solution code above by clicking on the little copy button on the bottom right of the codeblock below and paste it to the sql section of phpMyAdmin tool. Then click **Go**.

        ![image](./images/update_a2.1.png)


    4.  Copy the code below by clicking on the little copy button on the bottom right of the codeblock below and paste it to the sql section of phpMyAdmin tool. Then click **Go**.

        ```
        SELECT * FROM Instructor;
        ```


    5.  Your output resultset should look like the image below:

         ![image](./images/update_a2.2.png)



# 

## Task B: Practice exercises on UPDATE

Now, let us practice creating and running some UPDATE related queries.


1.  Problem:

    > *Update the city of the instructor record to Markham whose id is 1.*

     <details>
     <summary>Hint</summary>

    > Follow example 1 of the UPDATE exercise.

     </details>

     <details>
     <summary>Solution</summary>

    ```
    UPDATE Instructor 
    SET city='Markham' 
    WHERE ins_id=1;

    SELECT * FROM Instructor;
    ```


     </details>

     <details>
     <summary>Output</summary>

     ![image](./images/update_b1.png)

     </details>

2.  Problem:

    > *Update the city and country for Sandip with id 4 to Dhaka and BD respectively.*

     <details>
     <summary>Hint</summary>

    > Follow example 2 of the UPDATE exercise.

     </details>

     <details>
     <summary>Solution</summary>

    ```
    UPDATE Instructor 
    SET city='Dhaka', country='BD' 
    WHERE ins_id=4;

    SELECT * FROM Instructor;
    ```


     </details>

     <details>
     <summary>Output</summary>

     ![image](./images/update_b2.png)

     </details>


# Exercise 3: DELETE

In this exercise, you will first go through an example of using DELETE in a query and then solve an exercise problem by using it.

# 

## Task A: Example exercise on DELETE

Let us go through an example of a DELETE related query:


1.  In this example, we want to remove a row from the table.

    1.  Problem:

        > *Remove the instructor record of Doe whose id is 6.*

    2.  Solution:

        ```
        DELETE FROM instructor
        WHERE ins_id = 6;
        ```



    3.  Copy the solution code above by clicking on the little copy button on the bottom right of the codeblock below and paste it to the sql section of phpMyAdmin tool. Then click **Go**.

        ![image](./images/delete_a1.1.png)


    4.  Copy the code below by clicking on the little copy button on the bottom right of the codeblock below and paste it to the sql section of phpMyAdmin tool. Then click **Go**.

        ```
        SELECT * FROM Instructor;
        ```


    5.  Your output result set should look like the image below:

         ![image](./images/delete_a1.2.png)

# 

## Task B: Practice exercise on DELETE

Now, let us practice creating and running a DELETE related query.


1.  Problem:

    > *Remove the instructor record of Hima.*

     <details>
     <summary>Hint</summary>

    > Follow example 1 of the DELETE exercise.

     </details>

     <details>
     <summary>Solution</summary>

    ```
    DELETE FROM instructor
    WHERE firstname = 'Hima';

    SELECT * FROM Instructor;
    ```

     </details>

     <details>
     <summary>Output</summary>

     ![image](./images/delete_b1.png)


     </details>


# 

<h3> Congratulations! You have completed this Lab. <h3/>


# Author(s)

[Pratiksha Verma](https://www.linkedin.com/in/pratiksha-verma-6487561b1/)

## Changelog

| Date       | Version | Changed by      | Change Description      |
| ---------- | ------- | --------------- | ----------------------- |
| 2022-01-02 | 0.1     | Pratiksha verma |  Initial Version        |

## <h3 align="center"> IBM Corporation 2022. All rights reserved. <h3/>


